package kr.hs.yii.mate.fetcher;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

import kr.hs.yii.mate.R;

/**
 * Created by whdgm on 10/17/2016.
 */

public class SignUpTask extends AsyncTask<Intent,Void,String[]> {

    JSONObject mJson;
    Context mContext;
    AppCompatActivity activity;
    Intent intent;
    ProgressDialog dialog;
    AlertDialog errorDialog;
    SharedPreferences pref;
    SharedPreferences.Editor mEditor;
    Bundle args;

    public SignUpTask(AppCompatActivity activity, Context context){
        this.activity = activity;
        mContext = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = pref.edit();
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(mContext);
        dialog.setMessage("등록중...");
        dialog.show();
    }

    @Override
    protected String[] doInBackground(Intent... params) {

        args = params[0].getExtras();
        String[] rst = new String[3];
        intent = params[0];

        Connection mConn = Jsoup.connect("http://218.39.83.130/meal/signup.php");
        mConn.ignoreContentType(true)
                .data("name",args.getString("pref_name"))
                .data("address",args.getString("pref_address"))
                .data("birth",args.getString("pref_birth"))
                .data("phone",args.getString("pref_phone"));

        try {
            mJson = new JSONObject(mConn.execute().body());
            String cod = mJson.getString("cod");
            String msg = mJson.getString("msg");
            rst[0]=cod;
            rst[1]=msg;
            rst[2]=mJson.getString("userID");
            return rst;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return new String[]{"999","Json failed","-1"};
    }

    @Override
    protected void onPostExecute(String[] s) {
        if(dialog.isShowing()){
            dialog.dismiss();
            if(!s[0].equals("200")) {

                mEditor.putString("pref_name",args.getString("pref_name"))
                        .putString("pref_address",args.getString("pref_address"))
                        .putString("pref_birth",args.getString("pref_birth"))
                        .putString("pref_phone",args.getString("pref_phone"))
                        .putBoolean("pref_signed",args.getBoolean("pref_signed"));

                if(!args.getString("pref_parent_id").equals("")) {
                    mEditor.putString("pref_parent_id", args.getString("pref_parent_id"));
                }
                //mEditor.apply();
                mEditor.putString("pref_user_id",s[2]);
                mEditor.apply();

                mContext.startActivity(intent);
                activity.finish();
                return;
            }else {
                errorDialog = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle)
                        .setMessage("등록에 실패했습니다.")
                        .setPositiveButton("확인",null)
                        .create();
                errorDialog.show();
                return;
            }
        }
    }
}
