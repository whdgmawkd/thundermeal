package kr.hs.yii.mate.fetcher;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

/**
 * Created by whdgm on 10/20/2016.
 */

public class JoinSchedTask extends AsyncTask<Intent,Void,String[]> {

    private Context context;
    private Bundle args;

    public JoinSchedTask(Context context){
        this.context = context;
    }

    @Override
    protected String[] doInBackground(Intent... params) {
        String[] result = new String[2];
        args = params[0].getExtras();
        Connection conn = Jsoup.connect("http://218.39.83.130/meal/join.php")
                .ignoreContentType(true)
                .data("mealID",args.getString("mealID"))
                .data("userID",args.getString("userID"));
        try {
            JSONObject mJson = new JSONObject(conn.execute().body());
            result[0]=mJson.getString("cod");
            result[1]=mJson.getString("msg");
            return result;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return new String[]{"999","Unknown Error"};
    }

    @Override
    protected void onPostExecute(String[] strings) {
        if(strings[0].equals("200")) {
            Toast.makeText(context, "등록에 성공했습니다.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context,"등록에 실패했습니다.",Toast.LENGTH_SHORT).show();
        }
    }
}
