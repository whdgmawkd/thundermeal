package kr.hs.yii.mate.sched;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.hs.yii.mate.R;

/**
 * Created by whdgm on 8/25/2016.
 */
public class SchedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    ImageView imageView;
    TextView title, desc, time, loc;
    TextView id;

    private Context context;

    public SchedViewHolder(View itemView) {
        super(itemView);
        context=itemView.getContext();
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,SchedInfoActivity.class);
                intent.putExtra("id",id.getText().toString())
                        .putExtra("title",title.getText().toString())
                        .putExtra("desc",desc.getText().toString())
                        .putExtra("time",time.getText().toString())
                        .putExtra("loc",loc.getText().toString());

                context.startActivity(intent);
            }
        });
        imageView = (ImageView)itemView.findViewById(R.id.meal_item_image);
        title = (TextView)itemView.findViewById(R.id.meal_item_title);
        desc = (TextView)itemView.findViewById(R.id.meal_item_desc);
        time = (TextView)itemView.findViewById(R.id.meal_item_date);
        loc = (TextView)itemView.findViewById(R.id.meal_item_loc);

        id = (TextView)itemView.findViewById(R.id.meal_item_id);
    }

    @Override
    public void onClick(View v) {

    }
}
