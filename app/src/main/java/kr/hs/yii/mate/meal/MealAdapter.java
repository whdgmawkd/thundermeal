package kr.hs.yii.mate.meal;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kr.hs.yii.mate.R;

/**
 * Created by whdgm on 8/26/2016.
 */
public class MealAdapter extends RecyclerView.Adapter<MealViewHolder> {

    private List<MealItem> mList;
    private Context mContext;

    public MealAdapter(Context context, List<MealItem> list) {
        mList = list;
        mContext = context;
    }

    @Override
    public MealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //TODO: Load Item from json over http

        View mealView = inflater.inflate(R.layout.meal_item,parent,false);
        MealViewHolder viewHolder = new MealViewHolder(mealView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MealViewHolder holder, int position) {
        MealItem item = mList.get(position);

        TextView title = holder.title;
        TextView desc = holder.desc;
        TextView date = holder.time;
        TextView loc = holder.loc;
        TextView id = holder.id;
        ImageView imageView = holder.imageView;

        title.setText(item.getTitle());
        desc.setText(item.getDesc());
        date.setText(item.getDate());
        loc.setText(item.getLoc());
        id.setText(item.getID());
        imageView.setImageBitmap(item.getImage());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
