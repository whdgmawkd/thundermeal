package kr.hs.yii.mate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kr.hs.yii.mate.fetcher.MainListFetchTask;
import kr.hs.yii.mate.sched.SchedAdapter;
import kr.hs.yii.mate.sched.SchedItem;

/**
 * Created by whdgm on 8/22/2016.
 */
public class FragmentMain extends Fragment {

    private RecyclerView mRecycler;
    private List<SchedItem> mList = new ArrayList<>();
    private SchedAdapter mAdapter;

    @Override
    public void onResume() {
        super.onResume();
        fetchMealData();
        Log.d("frag","onResume");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,container,false);
        mRecycler = (RecyclerView)rootView.findViewById(R.id.recycler);

        mAdapter = new SchedAdapter(getContext(),mList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);

        fetchMealData();

        return rootView;
    }

    private void fetchMealData() {
        MainListFetchTask fetcher = new MainListFetchTask(mAdapter,mList);
        fetcher.execute();
    }
}
