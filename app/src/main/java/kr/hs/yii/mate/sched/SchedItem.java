package kr.hs.yii.mate.sched;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.URL;

/**
 * Created by whdgm on 8/26/2016.
 */
public class SchedItem {
    private String ID,title,desc,date,loc,imageURL;

    public SchedItem(String ID, String title, String desc, String date, String loc){
        this.ID = ID;
        this.title=title;
        this.desc=desc;
        this.date=date;
        this.loc=loc;
    }

    public String getDate() {
        return date;
    }

    public String getDesc() {
        return desc;
    }

    public String getID() {
        return ID;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getLoc() {
        return loc;
    }

    public String getTitle() {
        return title;
    }

    public Bitmap getImage(){
        Bitmap bitmap = null;
        Thread thread = new Thread(new getImageThread(bitmap));
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public class getImageThread implements Runnable{

        Bitmap target;

        public getImageThread(Bitmap target){
            this.target=target;
        }

        @Override
        public void run() {
            try {
                if(imageURL.equals("none")){
                    return;
                }
                target=BitmapFactory.decodeStream(new URL(getImageURL()).openStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
