package kr.hs.yii.mate;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import kr.hs.yii.mate.fetcher.SignUpTask;

public class SignUpActivity extends AppCompatActivity {

    private SharedPreferences mPref;
    private AlertDialog.Builder mDialogBuilder;
    private EditText eName,eAddress,eBirth,eParent;
    private TelephonyManager mTeleManager;
    private String phoneNo;
    private SignUpTask signUpTask;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mTeleManager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        phoneNo = mTeleManager.getLine1Number();
        eName=(EditText)findViewById(R.id.sign_name);
        eAddress=(EditText)findViewById(R.id.sign_address);
        eBirth=(EditText)findViewById(R.id.sign_birth);
        final Calendar mCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR,year);
                mCalendar.set(Calendar.MONTH,month);
                mCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                String dateFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                eBirth.setText(sdf.format(mCalendar.getTime()));
            }
        };
        eBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SignUpActivity.this
                        ,dateSetListener,mCalendar.get(Calendar.YEAR)
                        ,mCalendar.get(Calendar.MONTH)
                        ,mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        eParent=(EditText)findViewById(R.id.sign_parent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_sign_up_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.sign_up){
            signUpTask = new SignUpTask(this,this);
            SharedPreferences.Editor mEditor = mPref.edit();
            String name,birth,address,parent;
            name=eName.getText().toString();
            birth=eBirth.getText().toString();
            address=eAddress.getText().toString();
            parent=eParent.getText().toString();

            boolean isCompleted = true;

            if(eName.getText().toString().equals("")){
                eName.setError(getResources().getString(R.string.error_require));
                isCompleted=false;
            }
            if(eBirth.getText().toString().equals("")) {
                eBirth.setError(getResources().getString(R.string.error_require));
                isCompleted=false;
            }
            if(eAddress.getText().toString().equals("")){
                eAddress.setError(getResources().getString(R.string.error_require));
                isCompleted=false;
            }

            if(!isCompleted){
                return false;
            }

            mEditor.putString(getString(R.string.pref_name_key),name)
                    .putString(getString(R.string.pref_address_key),address)
                    .putString(getString(R.string.pref_birth_key),birth)
                    .putString(getString(R.string.pref_phone_key),phoneNo)
                    .putBoolean(getString(R.string.pref_signed_key),true);

            if(!parent.equals("")) {
                mEditor.putString(getResources().getString(R.string.pref_parent_key), parent);
            }
            mEditor.apply();

            Intent intent = new Intent(this,MainActivity.class).putExtra(getString(R.string.pref_name_key),name)
                    .putExtra(getString(R.string.pref_address_key),address)
                    .putExtra(getString(R.string.pref_birth_key),birth)
                    .putExtra(getString(R.string.pref_phone_key),phoneNo)
                    .putExtra(getString(R.string.pref_signed_key),true);
            if(!parent.equals("")) {
                intent.putExtra(getString(R.string.pref_parent_key),parent);
            } else {
                intent.putExtra(getString(R.string.pref_parent_key),"");
            }

            signUpTask.execute(intent);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setMessage("등록하지 않으면 사용할 수 없습니다.\n등록을 취소하고 종료하시겠습니까?");
        mDialogBuilder.setNegativeButton("아니요",null);
        mDialogBuilder.setPositiveButton("예", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).show();
    }
}
