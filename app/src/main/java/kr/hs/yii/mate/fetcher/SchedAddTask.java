package kr.hs.yii.mate.fetcher;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

/**
 * Created by whdgm on 10/20/2016.
 */

public class SchedAddTask extends AsyncTask<Intent,Void,String[]> {

    private Context context;
    private Bundle args;
    private String userID;
    private AppCompatActivity activity;

    public SchedAddTask(Context context, AppCompatActivity activity){
        this.context = context;
        this.activity = activity;
        userID = PreferenceManager.getDefaultSharedPreferences(context).getString("pref_user_id",null);

    }

    @Override
    protected String[] doInBackground(Intent... params) {
        String[] result = new String[2];
        args = params[0].getExtras();
        String loc,time,desc,count,title;
        loc=args.getString("loc");
        time=args.getString("time");
        desc=args.getString("desc");
        count=args.getString("count");
        title=args.getString("title");
        Connection conn = Jsoup.connect("http://218.39.83.130/meal/create.php")
                .ignoreContentType(true)
                .data("userID",userID)
                .data("loc",loc)
                .data("time",time)
                .data("desc",desc)
                .data("count",count)
                .data("title",title);
        try {
            JSONObject mJson = new JSONObject(conn.execute().body());
            result[0]=mJson.getString("cod");
            result[1]=mJson.getString("msg");
            return result;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return new String[]{"999","unknownError"};
    }

    @Override
    protected void onPostExecute(String[] strings) {
        if(strings[0].equals("201")) {
            activity.finish();
        } else {
            Toast.makeText(context, "등록에 실패했습니다.", Toast.LENGTH_SHORT).show();
        }
    }
}
