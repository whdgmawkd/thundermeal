package kr.hs.yii.mate.fetcher;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.List;

import kr.hs.yii.mate.sched.SchedAdapter;
import kr.hs.yii.mate.sched.SchedItem;

/**
 * Created by whdgm on 8/22/2016.
 */
public class MainListFetchTask extends AsyncTask<Void,Void,Void> {

    private SchedAdapter adapter;
    private List<SchedItem> mList;

    private String receiveRaw;
    private JSONObject receiveJSON;

    public MainListFetchTask(SchedAdapter adapter, List<SchedItem> list){
        this.adapter = adapter;
        mList = list;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        //new SchedItem(id,title,desc,date,loc,image)
        try {
            //TODO: Change Server method name to mate/schedList.php (was meal/mealList.php)
            receiveRaw = Jsoup.connect("http://218.39.83.130/mate/schedList.php").ignoreContentType(true).execute().body();
            receiveJSON = new JSONObject(receiveRaw);
            JSONArray array = receiveJSON.optJSONArray("item");
            mList.clear();
            for(int i=0;i<array.length();i++){
                JSONObject element = array.getJSONObject(i);
                mList.add(new SchedItem(element.getString("id"),element.getString("title"),
                        element.getString("desc"),element.getString("time"),element.getString("loc")));
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        adapter.notifyDataSetChanged();
    }
}
