package kr.hs.yii.mate.sched;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import kr.hs.yii.mate.R;
import kr.hs.yii.mate.fetcher.JoinSchedTask;

public class SchedInfoActivity extends AppCompatActivity {

    String id,title;
    TextView desc,time,loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sched_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        title = intent.getStringExtra("title");

        getSupportActionBar().setTitle(title);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, id, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent joinIntent = new Intent().putExtra("mealID",id).putExtra("userID",
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                .getString(getResources().getString(R.string.pref_user_key),null));
                JoinSchedTask task = new JoinSchedTask(getApplicationContext());
                task.execute(joinIntent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        desc = (TextView)findViewById(R.id.text_desc);
        time = (TextView)findViewById(R.id.text_time);
        loc = (TextView)findViewById(R.id.text_loc);

        desc.setText(intent.getStringExtra("desc"));
        time.setText(intent.getStringExtra("time"));
        loc.setText(intent.getStringExtra("loc"));

    }
}
